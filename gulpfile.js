var gulp = require('gulp'),
    concat = require('gulp-concat'),
    cssnano = require('cssnano'),
    postcss = require('gulp-postcss'),
    compass = require('gulp-compass'),
    watch = require('gulp-watch'),
    util = require('gulp-util'),
    babel = require('gulp-babel'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss = require('gulp-clean-css'),
    uglify = require('gulp-uglify');


var paths = {
    dest: 'web/dist',
    observe: ['web/styles/sass/**/*.scss', 'web/styles/js/**/*.js'],
    compassScss: ['web/styles/sass/main.scss'],
    compiledScss: ['web/dist/css/main.css'],
    scripts: [
        'web/styles/js/tabs.js',
        'web/styles/js/cards.js',
        'web/styles/js/chips.js',
        'web/styles/js/forms.js',
        'web/styles/js/modal.js',
        'web/styles/js/waves.js',
        'web/styles/js/global.js',
        'web/styles/js/slider.js',
        'web/styles/js/toasts.js',
        'web/styles/js/buttons.js',
        'web/styles/js/initial.js',
        'web/styles/js/pushpin.js',
        'web/styles/js/sideNav.js',
        'web/styles/js/tooltip.js',
        'web/styles/js/carousel.js',
        'web/styles/js/date_picker/picker.js',
        'web/styles/js/date_picker/picker.date.js',
        'web/styles/js/dropdown.js',
        'web/styles/js/parallax.js',
        'web/styles/js/animation.js',
        'web/styles/js/scrollspy.js',
        'web/styles/js/tapTarget.js',
        'web/styles/js/hammer.min.js',
        'web/styles/js/scrollFire.js',
        'web/styles/js/collapsible.js',
        'web/styles/js/materialbox.js',
        'web/styles/js/transitions.js',
        'web/styles/js/velocity.min.js',
        'web/styles/js/jquery.hammer.js',
        'web/styles/js/character_counter.js'
    ],
    fonts: ['web/styles/fonts/roboto/Roboto-{Bold,Light,Medium,Regular,Thin].{woff,woff2}']
};

var processors = [
    cssnano()
];

var dest = function (folder) {
    if (!folder) folder = '';
    return gulp.dest(paths.dest + '/' + folder);
};

gulp.task('compass', function () {
    return gulp.src('web/styles/sass/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer('last 1 version', '&gt; 1%', 'ie 8'))
        .pipe(minifyCss())
        .pipe(sourcemaps.write())
        .pipe(dest('css'));
});

gulp.task('styles', ['compass'], function () {
    return gulp.src(paths.compiledScss)
        .pipe(concat('main.min.css'))
        .pipe(postcss(processors))
        .pipe(dest('css'))
});

gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(concat('main.min.js'))
        .pipe(babel({"presets":["env"]}))
        .pipe(
            uglify().on(
                'error',
                function (err) {
                    util.log(util.colors.red('ERROR'), err.toString());
                }
            )
        )
        .pipe(dest('js'));
});

gulp.task('fonts', function () {
    return gulp.src(paths.fonts)
        .pipe(dest('fonts'))
});

gulp.task('watch', function() {
    return gulp.watch(paths.observe, ['default']);
});

gulp.task('watch:styles', function() {
    return gulp.watch(paths.observe[0], ['build:styles']);
});

gulp.task('watch:scripts', function() {
    return gulp.watch(paths.observe[0], ['build:scripts']);
});

gulp.task('default', ['styles', 'scripts', 'fonts']);
gulp.task('build:scripts', ['scripts']);
gulp.task('build:styles', ['styles', 'fonts']);
