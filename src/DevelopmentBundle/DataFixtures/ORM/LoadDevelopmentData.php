<?php

namespace Horiversum\TechTree\DevelopmentBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Horiversum\TechTree\CoreBundle\Entity\Planet;
use Horiversum\TechTree\CoreBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadDevelopmentData implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('admin')
            ->setPassword($this->container->get('security.password_encoder')->encodePassword($user, 'admin'))
            ->setApiKey(hash('sha256', 'Stefan83'))
            ->setSalt('')
            ->setRole('ROLE_ADMIN');

        $manager->persist($user);

        $manager->flush();
    }
}
