<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 15.08.17
 * Time: 22:32
 */

namespace Horiversum\TechTree\DeploymentBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;

class DeploymentExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load(__DIR__ . '/../Resources/config/rundeck.xml');
    }

}
