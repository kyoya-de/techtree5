<?php

namespace Horiversum\TechTree\DeploymentBundle\Command\Rundeck;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExecuteJobCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rundeck:job:execute')
            ->setDescription('Executes a Rundeck job.')
            ->addArgument('job-id', InputArgument::REQUIRED, 'ID of the Job to execute.')
            ->addArgument('job-options', InputArgument::IS_ARRAY, 'Options for Job execution (Format: <Option-Name>:<Option-Value>, Example: revision:1.4.2).');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $jobOptions = $this->normalizeJobOptions((array) $input->getArgument('job-options'));
        $rundeck = $this->getContainer()->get('horiversum.techtree.deployment.rundeck.client');
        $exitCode = 0;
        $rundeck
            ->job()
            ->run($input->getArgument('job-id'), $jobOptions)
            ->then(function (ResponseInterface $response) use ($output, &$exitCode) {
                $output->writeln(['<info>Job started</info>', (string) $response->getBody()]);
                $exitCode = 0;
            }, function (RequestException $exception) use ($output, &$exitCode) {
                $output->writeln(['<error>Request failed</error>', (string) $exception->getMessage()]);
                $exitCode = 255;
            })
            ->wait();

        return $exitCode;
    }

    /**
     * @param array $rawArguments
     *
     * @return array
     */
    private function normalizeJobOptions(array $rawArguments): array
    {
        $arguments = [];
        foreach ($rawArguments as $rawArgument) {
            list ($key, $value) = explode(':', $rawArgument, 2);
            $arguments[$key] = $value;
        }

        if (0 < count($arguments)) {
            $arguments = ['options' => $arguments];
        }

        return $arguments;
    }
}
