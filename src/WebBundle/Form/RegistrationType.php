<?php

namespace Horiversum\TechTree\WebBundle\Form;

use Horiversum\TechTree\CoreBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'Benutzername'])
            ->add('password', RepeatedType::class, [
                'second_options' => [
                    'label' => 'Passwort wiederholen',
                ],
                'first_options'  => [
                    'label' => 'Passwort',
                ],
                'first_name'     => 'password',
                'second_name'    => 'confirm',
                'type'           => PasswordType::class,'attr' => ['icon' => 'https'],
                'invalid_message' => 'Die Passwörter müssen übereinstimmen.',
            ])
            ->add('create', SubmitType::class, ['label' => 'Erstellen', 'attr' => ['class' => 'waves-effect btn']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'web_bundle_register_type';
    }
}
