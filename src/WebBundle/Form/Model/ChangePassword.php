<?php

namespace Horiversum\TechTree\WebBundle\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class ChangePassword
{
    /**
     * @var string
     *
     * @SecurityAssert\UserPassword(message="Das angegebene Passwort ist nicht korrekt!")
     */
    private $oldPassword = '';

    /**
     * @var string
     *
     * @Assert\Length(min="6", minMessage="Das Passwort muss mindestens 6 Zeichen lang sein!")
     */
    private $newPassword = '';

    /**
     * @return string
     */
    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     *
     * @return ChangePassword
     */
    public function setOldPassword(string $oldPassword): ChangePassword
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     *
     * @return ChangePassword
     */
    public function setNewPassword(string $newPassword): ChangePassword
    {
        $this->newPassword = $newPassword;

        return $this;
    }
}
