<?php

namespace Horiversum\TechTree\WebBundle\Form;

use Horiversum\TechTree\WebBundle\Form\Model\ChangePassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('oldPassword', PasswordType::class, ['label' => 'Altes Passwort'])
            ->add(
                'newPassword',
                RepeatedType::class,
                [
                    'second_options'  => [
                        'label' => 'Passwort wiederholen',
                    ],
                    'first_options'   => [
                        'label' => 'Passwort',
                    ],
                    'first_name'      => 'password',
                    'second_name'     => 'confirm',
                    'type'            => PasswordType::class,
                    'attr'            => ['icon' => 'https'],
                    'invalid_message' => 'Die Passwörter müssen übereinstimmen.',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => ChangePassword::class]);
    }
}
