<?php

namespace Horiversum\TechTree\WebBundle\Controller\User;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\OrderBy;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Horiversum\TechTree\CoreBundle\Entity\Building;
use Horiversum\TechTree\CoreBundle\Entity\Category;
use Horiversum\TechTree\CoreBundle\Entity\Item;
use Horiversum\TechTree\CoreBundle\Entity\Planet;
use Horiversum\TechTree\CoreBundle\Entity\User;
use InvalidArgumentException;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UnexpectedValueException;
use function array_filter;
use function in_array;

/**
 * Class PlanetController
 *
 * @package Horiversum\TechTree\WebBundle\Controller\User
 */
class PlanetController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('@Web/User/planets.html.twig');
    }

    /**
     * @param Request $request
     * @param Planet  $planet
     *
     * @return Response
     */
    public function savePlanetAction(Request $request, ?Planet $planet): Response
    {
        try {
            $this->checkPlanet($planet);

            $formData = $request->request->get('planet');
            $planet->setName($formData['name'])
                ->setType($formData['type'])
                ->setImageNo($formData['image'])
                ->setGalaxy($formData['galaxy'])
                ->setSystem($formData['system'])
                ->setPosition($formData['position']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($planet);
            $em->flush();
            $this->addFlash('info', 'Planet wurde bearbeitet.');
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('user_planets');
    }

    /**
     * @param Planet|null $planet
     *
     * @throws AccessDeniedException
     */
    private function checkPlanet(?Planet $planet)
    {
        /** @var User $user */
        $token = $this->get('security.token_storage')->getToken();
        if (!$token) {
            throw new AccessDeniedException('Sie müssen sich anmelden!');
        }
        $user = $token->getUser();

        if (!$planet || $planet->getUser()->getId() !== $user->getId()) {
            throw new AccessDeniedException('Dieser Planet existiert nicht oder gehört nicht zu deinem Account!');
        }
    }

    /**
     * @param Planet|null $planet
     *
     * @return Response
     */
    public function deletePlanetAction(?Planet $planet): Response
    {
        try {
            $this->checkPlanet($planet);

            $em = $this->getDoctrine()->getManager();

            foreach ($planet->getBuildings() as $building) {
                $em->remove($building);
            }

            $em->remove($planet);
            $em->flush();
            $this->addFlash('info', 'Planet wurde gelöscht.');
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('user_planets');
    }

    /**
     * @param Planet|null $planet
     *
     * @throws AccessDeniedException
     * @throws InvalidArgumentException
     * @throws LogicException
     * @throws UnexpectedValueException
     *
     * @return Response
     */
    public function detailsAction(?Planet $planet): Response
    {
        $this->checkPlanet($planet);

        /** @var EntityRepository $buildingRepo */
        $buildingRepo = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Building');

        $qb = $buildingRepo
            ->createQueryBuilder('b');
        $q  = $qb
            ->where('b.planet = :pid')
            ->setParameter('pid', $planet->getId())
            ->join(Item::class, 'i', Join::WITH, 'i.id = b.item')
            ->join(Category::class, 'c', Join::WITH, 'c.id = i.category')
            ->add('orderBy', new OrderBy('c.sort', 'ASC'), false)
            ->add('orderBy', new OrderBy('i.sort', 'ASC'), true)
            ->getQuery();

        /** @var Building[] $rawBuildings */
        $rawBuildings  = $q->getResult();
        $rawCategories = $this->getDoctrine()
            ->getRepository('CoreBundle:Category')
            ->findBy(['root' => 'building'], ['sort' => 'ASC']);

        /** @var Category[][] $categories */
        $categories = [];
        foreach ($rawCategories as $rawCategory) {
            $category = [
                'category' => $rawCategory,
                'items'    => [],
            ];

            foreach ($rawBuildings as $building) {
                if ($building->getItem()->getCategory()->getId() === $rawCategory->getId()) {
                    $category['items'][] = $building;
                }
            }
            if (0 < count($category['items'])) {
                $categories[] = $category;
            }
        }

        $pageTitle       = sprintf(
            '%s (%u:%u:%u)',
            $planet->getName(),
            $planet->getGalaxy(),
            $planet->getSystem(),
            $planet->getPosition()
        );
        $pageHeading     = sprintf('Gebäude auf dem Planeten %s', $pageTitle);
        $registeredItems = [];

        /** @var Building $building */
        foreach ($planet->getBuildings() as $building) {
            $registeredItems[] = $building->getItem()->getId();
        }

        return $this->render('@Web/user_items.html.twig', [
            'newItemDialog' => [
                'unregisteredItems' => $this->getUnregisteredItems('building', $registeredItems),
                'title'             => 'Neues Gebäude eintragen',
                'itemLabel'         => 'Gebäude',
            ],
            'categories'    => $categories,
            'pageTitle'     => $pageTitle,
            'pageHeading'   => $pageHeading,
            'pageRoutes'    => [
                'get'    => $this->generateUrl('user_planet_details', ['id' => $planet->getId()]),
                'create' => $this->generateUrl('user_planet_building_create', ['id' => $planet->getId()]),
                'edit'   => $this->generateUrl('user_planet_building_level', ['id' => $planet->getId()]),
                'delete' => $this->generateUrl('user_planet_building_delete', ['id' => $planet->getId()]),
            ],
        ]);
    }

    /**
     * @param $itemType
     * @param $registeredItems
     *
     * @return Item[]
     */
    private function getUnregisteredItems($itemType, $registeredItems): array
    {
        $em = $this->getDoctrine()->getManager();

        $itemRepo = $em->getRepository('CoreBundle:Item');
        /** @var QueryBuilder $qb */
        $qb = $itemRepo->createQueryBuilder('i');
        $q  = $qb
            ->join(Category::class, 'c', Join::WITH, 'c.id = i.category')
            ->where("c.root = '{$itemType}'")
            ->add('orderBy', new OrderBy('c.sort', 'ASC'))
            ->add('orderBy', new OrderBy('i.sort', 'ASC'), true)
            ->getQuery();

        /** @var Item[] $items */
        $items = $q->getResult();

        $possibleItems = array_filter($items, function ($item) use ($registeredItems) {
            return !in_array($item->getId(), $registeredItems, true);
        });

        /** @var Item[] $realItems */
        $realItems = [];

        foreach ($possibleItems as $possibleItem) {
            $realItems[$possibleItem->getCategory()->getTitle()][] = $possibleItem;
        }

        return $realItems;
    }

    /**
     * @param Request     $request
     * @param Planet|null $planet
     *
     * @return JsonResponse
     */
    public function setBuildingLevelAction(Request $request, ?Planet $planet): JsonResponse
    {
        $status = Response::HTTP_OK;
        $result = [
            'success' => true,
            'message' => 'Der Gebäudeeintrag wurde erfolgreich aktualisiert.',
        ];

        try {
            $this->checkPlanet($planet);

            $em       = $this->getDoctrine()->getManager();
            $building = $em->getRepository('CoreBundle:Building')->find($request->request->get('itemId'));

            if (!$building) {
                throw new AccessDeniedException('Dieses Gebäude existiert nicht!');
            }

            $building->setLevel($request->request->get('level'));
            $em->persist($building);
            $em->flush();
        } catch (AccessDeniedException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = Response::HTTP_FORBIDDEN;
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = 400;
        }

        return new JsonResponse($result, $status);
    }

    /**
     * @param Request     $request
     * @param Planet|null $planet
     *
     * @return JsonResponse
     */
    public function newBuildingAction(Request $request, ?Planet $planet): JsonResponse
    {
        $status = Response::HTTP_OK;
        $result = [
            'success' => true,
            'message' => 'Gebäudeeintrag erfolgreich erstellt.',
        ];

        try {
            $this->checkPlanet($planet);

            $em   = $this->getDoctrine()->getManager();
            $item = $em->getRepository('CoreBundle:Item')->find($request->request->get('itemId'));

            if (!$item) {
                throw new AccessDeniedException('Dieses Gebäude existiert nicht!');
            }

            $building = new Building();
            $building->setPlanet($planet);
            $building->setItem($item);
            $building->setLevel($request->request->get('itemLevel'));

            $em->persist($building);
            $em->flush();
        } catch (AccessDeniedException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = Response::HTTP_FORBIDDEN;
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = 400;
        }

        return new JsonResponse($result, $status);
    }

    /**
     * @param Request     $request
     * @param Planet|null $planet
     *
     * @return JsonResponse
     */
    public function deleteBuildingAction(Request $request, ?Planet $planet): JsonResponse
    {
        $status = Response::HTTP_OK;
        $result = [
            'success' => true,
            'message' => 'Gebäudeeintrag erfolgreich gelöscht.',
        ];

        try {
            $this->checkPlanet($planet);

            $em       = $this->getDoctrine()->getManager();
            $building = $em->getRepository('CoreBundle:Building')->find($request->request->get('itemId'));

            if (!$building) {
                throw new AccessDeniedException('Dieses Gebäude existiert nicht!');
            }

            $em->remove($building);
            $em->flush();
        } catch (AccessDeniedException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = Response::HTTP_FORBIDDEN;
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = 400;
        }

        return new JsonResponse($result, $status);
    }
}
