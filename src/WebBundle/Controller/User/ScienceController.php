<?php

namespace Horiversum\TechTree\WebBundle\Controller\User;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\OrderBy;
use Exception;
use Horiversum\TechTree\CoreBundle\Entity\Category;
use Horiversum\TechTree\CoreBundle\Entity\Item;
use Horiversum\TechTree\CoreBundle\Entity\Science;
use Horiversum\TechTree\CoreBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ScienceController extends Controller
{
    public function indexAction(): Response
    {
        $token = $this->get('security.token_storage')->getToken();
        if (!$token) {
            throw new AccessDeniedException('Sie müssen sich anmelden!');
        }

        /** @var User $user */
        $user = $token->getUser();

        /** @var EntityRepository $scienceRepo */
        $scienceRepo = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Science');

        $qb = $scienceRepo
            ->createQueryBuilder('s');
        $q  = $qb
            ->where('s.user = :userId')
            ->setParameter('userId', $user->getId())
            ->join(Item::class, 'i', Join::WITH, 'i.id = s.item')
            ->join(Category::class, 'c', Join::WITH, 'c.id = i.category')
            ->add('orderBy', new OrderBy('c.sort', 'ASC'), false)
            ->add('orderBy', new OrderBy('i.sort', 'ASC'), true)
            ->getQuery();

        /** @var Science[] $rawSciences */
        $rawSciences   = $q->getResult();
        $rawCategories = $this->getDoctrine()
            ->getRepository('CoreBundle:Category')
            ->findBy(['root' => 'research'], ['sort' => 'ASC']);

        /** @var Category[][] $categories */
        $categories = [];
        foreach ($rawCategories as $rawCategory) {
            $category = [
                'category' => $rawCategory,
                'items'    => [],
            ];

            foreach ($rawSciences as $science) {
                if ($science->getItem()->getCategory()->getId() === $rawCategory->getId()) {
                    $category['items'][] = $science;
                }
            }
            if (0 < count($category['items'])) {
                $categories[] = $category;
            }
        }

        $registeredItems = [];

        /** @var Science $science */
        foreach ($user->getSciences() as $science) {
            $registeredItems[] = $science->getItem()->getId();
        }

        return $this->render('@Web/user_items.html.twig', [
            'newItemDialog' => [
                'unregisteredItems' => $this->getUnregisteredItems('research', $registeredItems),
                'title'             => 'Neue Forschung eintragen',
                'itemLabel'         => 'Forschung',
            ],
            'categories'    => $categories,
            'pageTitle'     => 'Forschungen',
            'pageHeading'   => 'Forschungen',
            'pageRoutes'    => [
                'get'    => $this->generateUrl('user_sciences'),
                'create' => $this->generateUrl('user_science_create'),
                'edit'   => $this->generateUrl('user_science_edit'),
                'delete' => $this->generateUrl('user_science_delete'),
            ],
        ]);
    }

    /**
     * @param $itemType
     * @param $registeredItems
     *
     * @return Item[]
     */
    private function getUnregisteredItems($itemType, $registeredItems): array
    {
        $em = $this->getDoctrine()->getManager();

        $itemRepo = $em->getRepository('CoreBundle:Item');
        /** @var QueryBuilder $qb */
        $qb = $itemRepo->createQueryBuilder('i');
        $q  = $qb
            ->join(Category::class, 'c', Join::WITH, 'c.id = i.category')
            ->where("c.root = '{$itemType}'")
            ->add('orderBy', new OrderBy('c.sort', 'ASC'))
            ->add('orderBy', new OrderBy('i.sort', 'ASC'), true)
            ->getQuery();

        /** @var Item[] $items */
        $items = $q->getResult();

        $possibleItems = array_filter($items, function ($item) use ($registeredItems) {
            return !in_array($item->getId(), $registeredItems, true);
        });

        /** @var Item[] $realItems */
        $realItems = [];

        foreach ($possibleItems as $possibleItem) {
            $realItems[$possibleItem->getCategory()->getTitle()][] = $possibleItem;
        }

        return $realItems;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setScienceLevelAction(Request $request): JsonResponse
    {
        $status = Response::HTTP_OK;
        $result = [
            'success' => true,
            'message' => 'Der Forschungseintrag wurde erfolgreich aktualisiert.',
        ];

        try {

            $em      = $this->getDoctrine()->getManager();
            $science = $em->getRepository('CoreBundle:Science')->find($request->request->get('itemId'));
            $this->checkScience($science);

            if (!$science) {
                throw new AccessDeniedException('Diese Forschung existiert nicht!');
            }

            $science->setLevel($request->request->get('level'));
            $em->persist($science);
            $em->flush();
        } catch (AccessDeniedException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = Response::HTTP_FORBIDDEN;
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = 400;
        }

        return new JsonResponse($result, $status);
    }

    /**
     * @param Request     $request
     *
     * @return JsonResponse
     */
    public function createScienceAction(Request $request): JsonResponse
    {
        $status = Response::HTTP_OK;
        $result = [
            'success' => true,
            'message' => 'Forschungseintrag erfolgreich erstellt.',
        ];

        try {

            $em   = $this->getDoctrine()->getManager();
            $item = $em->getRepository('CoreBundle:Item')->find($request->request->get('itemId'));

            if (!$item) {
                throw new AccessDeniedException('Diese Forschung existiert nicht!');
            }

            /** @var User $user */
            $token = $this->get('security.token_storage')->getToken();
            if (!$token) {
                throw new AccessDeniedException('Sie müssen sich anmelden!');
            }

            $building = new Science();
            $building->setUser($token->getUser());
            $building->setItem($item);
            $building->setLevel($request->request->get('itemLevel'));

            $em->persist($building);
            $em->flush();
        } catch (AccessDeniedException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = Response::HTTP_FORBIDDEN;
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = 400;
        }

        return new JsonResponse($result, $status);
    }

    /**
     * @param Science|null $science
     *
     * @throws AccessDeniedException
     */
    private function checkScience(?Science $science)
    {
        /** @var User $user */
        $token = $this->get('security.token_storage')->getToken();
        if (!$token) {
            throw new AccessDeniedException('Sie müssen sich anmelden!');
        }
        $user = $token->getUser();

        if (!$science || $science->getUser()->getId() !== $user->getId()) {
            throw new AccessDeniedException('Diese Forschung existiert nicht oder gehört nicht zu deinem Account!');
        }
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteScienceAction(Request $request): JsonResponse
    {
        $status = Response::HTTP_OK;
        $result = [
            'success' => true,
            'message' => 'Forschungseintrag erfolgreich gelöscht.',
        ];

        try {

            $em      = $this->getDoctrine()->getManager();
            $science = $em->getRepository('CoreBundle:Science')->find($request->request->get('itemId'));

            $this->checkScience($science);

            if (!$science) {
                throw new AccessDeniedException('Diese Forschung existiert nicht!');
            }

            $em->remove($science);
            $em->flush();
        } catch (AccessDeniedException $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = Response::HTTP_FORBIDDEN;
        } catch (Exception $e) {
            $result['success'] = false;
            $result['message'] = $e->getMessage();
            $status            = 400;
        }

        return new JsonResponse($result, $status);
    }
}
