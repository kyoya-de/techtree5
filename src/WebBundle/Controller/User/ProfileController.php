<?php

namespace Horiversum\TechTree\WebBundle\Controller\User;

use Horiversum\TechTree\CoreBundle\Entity\User;
use Horiversum\TechTree\WebBundle\Form\ChangePasswordType;
use Horiversum\TechTree\WebBundle\Form\Model\ChangePassword;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $form = $this->createForm(
            ChangePasswordType::class,
            new ChangePassword(),
            [
                'action' => $this->generateUrl('user_change_password'),
                'attr'   => ['class' => 'ajax-form'],
            ]
        );

        return $this->render('@Web/User/profile.html.twig', ['changePasswordForm' => $form->createView()]);
    }

    public function generateApiKeyAction()
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $apiKey = $this->get('random_string_generator')->generate(16);
        $user->setApiKey($apiKey);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse(['key' => $apiKey]);
    }

    public function changePasswordAction(Request $request)
    {
        $form = $this->createForm(ChangePasswordType::class, new ChangePassword());
        $form->handleRequest($request);

        $allErrors = [];
        $success   = false;
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                /** @var ChangePassword $changePassword */
                $changePassword  = $form->getData();
                $user            = $this->get('security.token_storage')->getToken()->getUser();
                $passwordEncoder = $this->get('security.password_encoder');

                $user->setSalt(base64_encode(\Sodium\randombytes_buf(16)));
                $password = $passwordEncoder->encodePassword($user, $changePassword->getNewPassword());
                $user->setPassword($password);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $success = true;
            } else {
                $oldPassword = $form['oldPassword'];
                if (0 < ($errors = $oldPassword->getErrors())->count()) {
                    $allErrors['oldPassword'] = $errors[0]->getMessage();
                }

                $newPassword = $form['newPassword']['password'];
                if (0 < ($errors = $newPassword->getErrors())->count()) {
                    $allErrors['newPassword'] = $errors[0]->getMessage();
                }

                $success = false;
            }
        }

        return new JsonResponse(['success' => $success, 'errors' => $allErrors]);
    }
}
