<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 26.06.17
 * Time: 20:21
 */

namespace Horiversum\TechTree\WebBundle\Controller;

use Horiversum\TechTree\CoreBundle\Entity\Item;
use Horiversum\TechTree\CoreBundle\Repository\RequirementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ItemController extends Controller
{
    public function listAction($type): Response
    {
        $em         = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('CoreBundle:Category')->findBy(['root' => $type]);

        return $this->render(
            '@Web/list.html.twig',
            [
                'type'       => $type,
                'categories' => $categories,
            ]
        );
    }

    public function itemDetailsAction(Item $item): Response
    {
        /** @var RequirementRepository $reqRepo */
        $reqRepo = $this->getDoctrine()->getManager()->getRepository('CoreBundle:Requirement');

        $levels          = $reqRepo->getItemLevels($item);
        $absRequirements = [];
        foreach ($levels as $level) {
            $absRequirements[$level] = $reqRepo->getAbsoluteRequirements($item, $level);
        }

        return $this->render(
            '@Web/item_details.html.twig',
            [
                'item'         => $item,
                'requirements' => $absRequirements,
            ]
        );
    }
}
