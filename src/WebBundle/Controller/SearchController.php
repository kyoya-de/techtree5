<?php

namespace Horiversum\TechTree\WebBundle\Controller;

use Horiversum\TechTree\CoreBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{

    public function indexAction(Request $request)
    {
        $term = $request->get('term');

        $search    = $this->get('horiversum.search');
        $rawResult = $search->search($term);

        /** @var Item[][] $result */
        $result = [
            'building' => [],
            'research' => [],
            'ship'     => [],
            'defense'  => [],
        ];
        foreach ($rawResult as $item) {
            $result[$item->getType()][] = $item;
        }

        return $this->render(
            '@Web/search.html.twig',
            [
                'term'          => $term,
                'searchResults' => $result,
            ]
        );
    }
}
