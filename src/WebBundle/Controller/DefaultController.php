<?php

namespace Horiversum\TechTree\WebBundle\Controller;

use Horiversum\TechTree\CoreBundle\Entity\User;
use Horiversum\TechTree\WebBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use function file_exists;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $changeLog     = [];
        $changeLogFile = $this->getParameter('kernel.root_dir') . '/ChangeLog.php';
        if (file_exists($changeLogFile)) {
            $changeLog = include $changeLogFile;
        }

        return $this->render('@Web/Default/index.html.twig', ['changeLog' => $changeLog]);
    }

    public function loginAction()
    {
        $authUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('@Web/Default/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    public function registerAction(Request $request)
    {
        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);

        $recaptchaValid = true;

        if ($this->validateRegistration($request, $form)) {
            /** @var User $user */
            $user = $form->getData();
            $user->setSalt(base64_encode(\Sodium\randombytes_buf(16)));
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        $viewVariables = [
            'form'             => $form->createView(),
            'recaptchaSiteKey' => $this->getParameter('recaptcha_website'),
            'recaptchaValid'   => $recaptchaValid,

        ];

        return $this->render(
            '@Web/Default/register.html.twig',
            $viewVariables
        );
    }

    /**
     * @param Request $request
     * @param Form    $form
     *
     * @return bool
     */
    private function validateRegistration(Request $request, $form): bool
    {
        return $form->isSubmitted() &&
            $form->isValid() &&
            $recaptchaValid = $this->get('recaptcha.client')->validate($request->get('g-recaptcha-response'));
    }
}
