<?php

namespace Horiversum\TechTree\WebBundle\Services\Recaptcha;

use GuzzleHttp\ClientInterface;

class Client
{
    private $client;

    private $secret;

    public function __construct(ClientInterface $client, string $secret)
    {
        $this->client = $client;
        $this->secret = $secret;
    }

    public function validate($response): bool
    {
        $response = $this->client->request(
            'POST',
            'siteverify',
            ['form_params' => ['secret' => $this->secret, 'response' => $response]]
        );

        return \GuzzleHttp\json_decode((string) $response->getBody())->success;
    }
}
