<?php

namespace Horiversum\TechTree\WebBundle\Services;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class GuzzleFactory
{
    public static function createGuzzle(?string $baseUri = null): ClientInterface
    {
        $options = [];

        if (null !== $baseUri) {
            $options = ['base_uri' => $baseUri];
        }

        return new Client($options);
    }
}
