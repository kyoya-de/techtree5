<?php

namespace Horiversum\TechTree\WebBundle\Twig;

use Horiversum\TechTree\CoreBundle\Entity\Planet;
use Twig_Extension;
use Twig_SimpleFunction;
use function strtolower;
use function strtoupper;
use function strtr;

class PlanetExtension extends Twig_Extension
{
    private $imagePaths = [
        'small'  => 'http://horiversum.org/game/pix/skins/default/planets/{type:upper}/pl_btn_{imageNo}.png',
        'medium' => 'http://horiversum.org/game/pix/skins/default/planets/{type:upper}/pl_m_{imageNo}.png',
        'large'  => 'http://horiversum.org/game/pix/skins/default/planets/{type:upper}/pl_big_{imageNo}.gif',
        'big'    => 'http://horiversum.org/game/pix/skins/default/planets/description/{type:lower}.jpg',
    ];

    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('planet_image', [$this, 'imageUrl']),
            new Twig_SimpleFunction('planet_coords', [$this, 'coordinates']),
        ];
    }

    public function imageUrl(Planet $planet, $imageSize = 'medium')
    {
        $typeReplacements = [
            '{type:upper}' => strtoupper($planet->getType()),
            '{type:lower}' => strtolower($planet->getType()),
            '{imageNo}'    => $planet->getImageNo(),
        ];

        return strtr($this->imagePaths[$imageSize], $typeReplacements);
    }

    public function coordinates(Planet $planet)
    {
        return "{$planet->getGalaxy()}:{$planet->getSystem()}:{$planet->getPosition()}";
    }
}
