<?php

namespace Horiversum\TechTree\WebBundle\Twig;

use Horiversum\TechTree\CoreBundle\Entity\Item;
use Twig_Extension;
use Twig_SimpleFunction;
use function strtolower;
use function strtoupper;
use function strtr;

class ItemExtension extends Twig_Extension
{
    private static $imagePaths = [
        'building' => 'http://horiversum.org/game/pix/skins/default/cnt/buildings/bld_{name:lower}.jpg',
        'research' => 'http://horiversum.org/game/pix/skins/default/cnt/research/rsh_{name:lower}.jpg',
        'ship'     => 'http://horiversum.org/game/pix/skins/default/cnt/ships/shp_{name:lower}.jpg',
        'defense'  => 'http://horiversum.org/game/pix/skins/default/cnt/defence/def_{name:lower}.jpg',
    ];

    private static $typeTitles = [
        'building' => 'Gebäude',
        'research' => 'Forschungen',
        'ship'     => 'Raumschiffe/Sonden',
        'defense'  => 'Verteidigungsanlagen',
    ];

    public function getFunctions(): array
    {
        return [
            new Twig_SimpleFunction('item_image', [$this, 'imageUrl']),
            new Twig_SimpleFunction('type_title', [$this, 'typeTitle']),
            new Twig_SimpleFunction('item_type_title', [$this, 'itemTypeTitle']),
        ];
    }

    public function imageUrl(Item $item): string
    {
        $itemName = $item->hasImage() ? $item->getName() : 'blind';

        $nameReplacements = [
            '{name:upper}' => strtoupper($itemName),
            '{name:lower}' => strtolower($itemName),
        ];

        return strtr(self::$imagePaths[$item->getType()], $nameReplacements);
    }

    public function typeTitle(string $type)
    {
        return self::$typeTitles[$type];
    }

    public function itemTypeTitle(Item $item)
    {
        return $this->typeTitle($item->getType());
    }
}
