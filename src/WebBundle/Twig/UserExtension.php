<?php

namespace Horiversum\TechTree\WebBundle\Twig;

use Horiversum\TechTree\CoreBundle\Entity\User;
use Twig_Extension;
use Twig_SimpleFunction;

class UserExtension extends Twig_Extension
{
    public function getFunctions(): array
    {
        return [
            new Twig_SimpleFunction('user_gradient', [$this, 'getProfileGradient'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('user_gravatar', [$this, 'getGravatarUrl']),
        ];
    }

    public function getProfileGradient(User $user): string
    {
        $color = array_reduce(
            str_split(hash('sha384', $user->getUsername()), 6),
            function ($acc, $item) {
                return $acc ^ hexdec($item);
            },
            0
        );

        $baseColor     = sprintf('%06X', $color);
        $darkenedColor = implode(
            '',
            array_map(
                function ($e) {
                    return sprintf('%02X', hexdec($e) * 0.5);
                },
                str_split($baseColor, 2)
            )
        );

        return sprintf('linear-gradient(180deg, #%s, #%s)', $baseColor, $darkenedColor);
    }

    public function getGravatarUrl(User $user, int $size = 100): string
    {
        return sprintf('https://www.gravatar.com/avatar/%s?s=%u&d=identicon', hash('md5', $user->getUsername()), $size);
    }
}
