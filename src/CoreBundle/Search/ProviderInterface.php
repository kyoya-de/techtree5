<?php

namespace Horiversum\TechTree\CoreBundle\Search;

interface ProviderInterface
{
    public function search(string $term): array;
}
