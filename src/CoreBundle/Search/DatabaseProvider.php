<?php

namespace Horiversum\TechTree\CoreBundle\Search;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\OrderBy;

class DatabaseProvider implements ProviderInterface
{
    private $registry;

    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    public function search(string $term): array
    {
        $qb      = $this->registry->getManager()->getRepository('CoreBundle:Item')->createQueryBuilder('i');
        $orderBy = new OrderBy();
        $orderBy->add('i.type', 'ASC');
        $orderBy->add('c.sort', 'ASC');
        $orderBy->add('i.sort', 'ASC');

        $q = $qb
            ->where('i.name LIKE :term')
            ->join('i.category', 'c')
            ->orWhere('i.title LIKE :term')
            ->setParameter(':term', "%{$term}%")
            ->orderBy($orderBy)
            ->getQuery();

        return $q->getResult();
    }
}
