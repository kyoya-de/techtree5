<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 30.06.17
 * Time: 21:04
 */

namespace Horiversum\TechTree\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Horiversum\TechTree\CoreBundle\Entity\Item;
use Horiversum\TechTree\CoreBundle\Entity\Requirement;

class RequirementRepository extends EntityRepository
{
    public function getItemLevels($item): array
    {
        $query = $this
            ->getEntityManager()
            ->createQuery('SELECT DISTINCT r.itemLevel FROM ' . Requirement::class . ' r WHERE r.item = :item')
            ->setParameter('item', $item);

        return array_map('intval', array_map('reset', $query->getScalarResult()));
    }

    public function getAbsoluteRequirements(Item $item, int $itemLevel): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Requirement::class, 'rr')
            ->addFieldResult('rr', 'id', 'id')
            ->addFieldResult('rr', 'item_level', 'itemLevel')
            ->addFieldResult('rr', 'required_item_level', 'requiredItemLevel')
            ->addJoinedEntityResult(Item::class, 'i', 'rr', 'item')
            ->addFieldResult('i', 'item_id', 'id')
            ->addJoinedEntityResult(Item::class, 'ri', 'rr', 'requiredItem')
            ->addFieldResult('ri', 'required_item_id', 'id')
            ->addFieldResult('ri', 'required_item_name', 'name')
            ->addFieldResult('ri', 'required_item_title', 'title')
            ->addFieldResult('ri', 'required_item_image', 'image')
            ->addFieldResult('ri', 'required_item_type', 'type')
        ;

        $query = $this
            ->getEntityManager()
            ->createNativeQuery(
                'SELECT
                        rr.id,
                        rr.item_id,
                        rr.item_level,
                        rr.required_item_level,
                        rr.required_item_id,
                        ri.name required_item_name,
                        ri.title required_item_title,
                        ri.has_image required_item_image,
                        ri.item_type required_item_type
                    FROM requirement rr, item ri, (
                      SELECT
                        gr.item_id,
                        gr.required_item_id,
                        MAX(gr.required_item_level) required_item_level
                      FROM requirement gr
                      WHERE gr.item_id = :itemId AND gr.item_level <= :itemLevel
                      GROUP BY gr.item_id, gr.required_item_id
                      ) r
                    WHERE
                        rr.item_id = r.item_id AND
                        rr.required_item_id = r.required_item_id AND
                        rr.required_item_level = r.required_item_level AND
                        ri.id = rr.required_item_id
                    ORDER BY ri.item_type ASC, ri.title ASC',
                $rsm
            );

        return $query->setParameter('itemId', $item)->setParameter('itemLevel', $itemLevel)->getResult();
    }
}
