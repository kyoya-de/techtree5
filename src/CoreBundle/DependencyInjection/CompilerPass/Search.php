<?php

namespace Horiversum\TechTree\CoreBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Horiversum\TechTree\CoreBundle\Service\Search as HoriversumSearch;
use Symfony\Component\DependencyInjection\Reference;

class Search implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has('horiversum.search')) {
            return;
        }

        $definition = $container->findDefinition('horiversum.search');

        $taggedServices = $container->findTaggedServiceIds('horiversum.search.provider');

        /**
         * @var string $id
         * @var array $tags
         */
        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addProvider', [new Reference($id), $attributes['priority']]);
            }
        }
    }

}
