<?php

namespace Horiversum\TechTree\CoreBundle\DependencyInjection;

use Horiversum\TechTree\CoreBundle\Search\ProviderInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class CoreExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(ProviderInterface::class)->addTag('horiversum.search.provider');

        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load(__DIR__ . '/../Resources/config/request.xml');
        $loader->load(__DIR__ . '/../Resources/config/search.xml');
    }

}
