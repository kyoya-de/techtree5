<?php

namespace Horiversum\TechTree\CoreBundle\Command\Item;

use Horiversum\TechTree\CoreBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function curl_getinfo;

class UpdateImagesCommand extends ContainerAwareCommand
{
    private static $baseUrls = [
        'building' => 'http://horiversum.org/game/pix/skins/default/cnt/buildings/bld_{name}.jpg',
        'research' => 'http://horiversum.org/game/pix/skins/default/cnt/research/rsh_{name}.jpg',
        'ship'     => 'http://horiversum.org/game/pix/skins/default/cnt/ships/shp_{name}.jpg',
        'defense'  => 'http://horiversum.org/game/pix/skins/default/cnt/defence/def_{name}.jpg',
    ];

    protected function configure()
    {
        $this->setName('tt:item:update-images');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $output->write('Fetching Items...');
        /** @var Item[] $items */
        $items = $em->getRepository('CoreBundle:Item')->findAll();
        $output->writeln('<info>Done</info>');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_NOBODY, true);

        $progress = new ProgressBar($output, count($items));
        $progress->setBarWidth(120);
        $progress->setFormat('very_verbose');

        foreach ($items as $item) {
            $url = str_replace(
                '{name}',
                strtolower($item->getName()),
                self::$baseUrls[$item->getCategory()->getRoot()]
            );

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $item->setImage(200 === $httpCode);
            $em->persist($item);

            $progress->advance();
            usleep(100000);
        }

        $progress->finish();

        $output->writeln('');

        $em->flush();

        curl_close($ch);
    }

}
