<?php

namespace Horiversum\TechTree\CoreBundle;

use Horiversum\TechTree\CoreBundle\DependencyInjection\CompilerPass\Search;
use Horiversum\TechTree\CoreBundle\Security\Factory\WsseFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new Search());
        parent::build($container);

        /** @var SecurityExtension $extension */
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new WsseFactory());
    }
}
