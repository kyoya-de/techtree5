<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 23.05.17
 * Time: 21:56
 */

namespace Horiversum\TechTree\CoreBundle\Security\Authentication\Provider;

use Horiversum\TechTree\CoreBundle\Entity\User;
use Horiversum\TechTree\CoreBundle\Security\Authentication\Token\WsseUserToken;
use InvalidArgumentException;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException as CacheInvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class WsseProvider implements AuthenticationProviderInterface
{
    private $userProvider;
    private $cachePool;

    public function __construct(UserProviderInterface $userProvider, CacheItemPoolInterface $cachePool)
    {
        $this->userProvider = $userProvider;
        $this->cachePool    = $cachePool;
    }

    /**
     * @param TokenInterface $token
     *
     * @throws UsernameNotFoundException
     * @throws AuthenticationException
     * @throws CacheInvalidArgumentException
     * @throws NonceExpiredException
     * @throws InvalidArgumentException
     *
     * @return WsseUserToken
     */
    public function authenticate(TokenInterface $token): WsseUserToken
    {
        /** @var WsseUserToken $token */
        /** @var User $user */
        $user = $this->userProvider->loadUserByUsername($token->getUsername());

        if ($user && $this->validateDigest($token->digest, $token->nonce, $token->created, $user->getApiKey())) {
            $authenticatedToken = new WsseUserToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }

        throw new AuthenticationException('The WSSE authentication failed.');
    }

    /**
     * This function is specific to Wsse authentication and is only used to help this example
     *
     * For more information specific to the logic here, see
     * https://github.com/symfony/symfony-docs/pull/3134#issuecomment-27699129
     */
    /**
     * @param string $digest
     * @param string $nonce
     * @param string $created
     * @param string $secret
     *
     * @throws NonceExpiredException
     * @throws CacheInvalidArgumentException
     * @return bool
     */
    protected function validateDigest($digest, $nonce, $created, $secret): bool
    {
        // Check created time is not in the future (based on minutes, to prevent inaccurate server time)
        $creationTime = strtotime($created);
        if (($creationTime - ($creationTime % 60)) > time()) {
            return false;
        }

        // Expire timestamp after 5 minutes
        if (time() - strtotime($created) > 300000) {
            return false;
        }

        // Try to fetch the cache item from pool
        $cacheItem = $this->cachePool->getItem(md5($nonce));

        // Validate that the nonce is *not* in cache
        // if it is, this could be a replay attack
        if ($cacheItem->isHit()) {
            throw new NonceExpiredException('Previously used nonce detected');
        }

        // Store the item in cache for 5 minutes
        $cacheItem->set(null)->expiresAfter(1);
        $this->cachePool->save($cacheItem);

        // Validate Secret
        $token    = base64_decode($nonce) . $created . $secret;
        $expected = base64_encode(sha1($token, true));

        return hash_equals($expected, $digest);
    }

    public function supports(TokenInterface $token): bool
    {
        return $token instanceof WsseUserToken;
    }
}
