<?php

namespace Horiversum\TechTree\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requirement
 *
 * @ORM\Table(
 *     name="requirement",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uniqReqPerItem", columns={"item_id", "required_item_id", "required_item_level"}),
 *         @ORM\UniqueConstraint(name="uniqReqPerLevel", columns={"item_id", "item_level", "required_item_id"})
 *     },
 *     indexes={@ORM\Index(columns={"item_level"})}
 * )
 * @ORM\Entity(repositoryClass="Horiversum\TechTree\CoreBundle\Repository\RequirementRepository")
 */
class Requirement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Item", inversedBy="requirements")
     */
    private $item;

    /**
     * @var int
     *
     * @ORM\Column(name="item_level", type="smallint")
     */
    private $itemLevel;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Item", inversedBy="prerequisiteFor")
     */
    private $requiredItem;

    /**
     * @var int
     *
     * @ORM\Column(name="required_item_level", type="smallint")
     */
    private $requiredItemLevel;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Item
     */
    public function getItem(): Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     *
     * @return Requirement
     */
    public function setItem($item): Requirement
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return Item
     */
    public function getRequiredItem(): Item
    {
        return $this->requiredItem;
    }

    /**
     * @param Item $requiredItem
     *
     * @return Requirement
     */
    public function setRequiredItem($requiredItem): Requirement
    {
        $this->requiredItem = $requiredItem;

        return $this;
    }

    /**
     * @return int
     */
    public function getItemLevel(): int
    {
        return $this->itemLevel;
    }

    /**
     * @param int $itemLevel
     *
     * @return Requirement
     */
    public function setItemLevel(int $itemLevel): Requirement
    {
        $this->itemLevel = $itemLevel;

        return $this;
    }

    /**
     * @return int
     */
    public function getRequiredItemLevel(): int
    {
        return $this->requiredItemLevel;
    }

    /**
     * @param int $requiredItemLevel
     *
     * @return Requirement
     */
    public function setRequiredItemLevel(int $requiredItemLevel): Requirement
    {
        $this->requiredItemLevel = $requiredItemLevel;

        return $this;
    }
}

