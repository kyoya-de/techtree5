<?php

namespace Horiversum\TechTree\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Planet
 *
 * @ORM\Table(name="planet", uniqueConstraints={@ORM\UniqueConstraint(name="unique_coords", columns={"galaxy", "system", "position"})})
 * @ORM\Entity()
 */
class Planet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\User", inversedBy="planets")
     */
    private $user;

    /**
     * @var Building[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Building", mappedBy="planet")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $buildings;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="galaxy", type="smallint")
     */
    private $galaxy;

    /**
     * @var int
     *
     * @ORM\Column(name="system", type="smallint")
     */
    private $system;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="smallint")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=4)
     */
    private $type = 'stpl';

    /**
     * @var int
     *
     * @ORM\Column(name="image_no", type="smallint")
     */
    private $imageNo = 0;

    public function __construct()
    {
        $this->buildings = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Planet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get galaxy
     *
     * @return int
     */
    public function getGalaxy()
    {
        return $this->galaxy;
    }

    /**
     * Set galaxy
     *
     * @param integer $galaxy
     *
     * @return Planet
     */
    public function setGalaxy($galaxy)
    {
        $this->galaxy = $galaxy;

        return $this;
    }

    /**
     * Get system
     *
     * @return int
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * Set system
     *
     * @param integer $system
     *
     * @return Planet
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Planet
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Planet
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Planet
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return ArrayCollection|Building[]
     */
    public function getBuildings()
    {
        return $this->buildings;
    }

    /**
     * @param ArrayCollection|Building[] $buildings
     * @return Planet
     */
    public function setBuildings($buildings)
    {
        $this->buildings = $buildings;

        return $this;
    }

    /**
     * @param Building $building
     * @return $this
     */
    public function addAvailability(Building $building)
    {
        $this->buildings->add($building);

        return $this;
    }

    /**
     * @param Building $building
     * @return $this
     */
    public function removeAvailability(Building $building)
    {
        $this->buildings->removeElement($building);

        return $this;
    }

    /**
     * @return int
     */
    public function getImageNo()
    {
        return $this->imageNo;
    }

    /**
     * @param int $imageNo
     * @return Planet
     */
    public function setImageNo($imageNo)
    {
        $this->imageNo = $imageNo;

        return $this;
    }
}
