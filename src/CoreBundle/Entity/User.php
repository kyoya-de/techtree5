<?php

namespace Horiversum\TechTree\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity()
 * @UniqueEntity(fields="username", message="Der Benutzername ist bereits vergeben!")
 */
class User implements AdvancedUserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Planet[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Planet", mappedBy="user")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     * @ORM\OrderBy({"galaxy" = "ASC", "system" = "ASC", "position" = "ASC"})
     */
    private $planets;

    /**
     * @var Science[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Science", mappedBy="user")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $sciences;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=64, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\Length(min="6", minMessage="Das Passwort muss mindestens 6 Zeichen lang sein!")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=20)
     */
    private $role = 'ROLE_USER';

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=128)
     */
    private $apiKey = '';

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->planets = new ArrayCollection();
        $this->sciences = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return ArrayCollection|Planet[]
     */
    public function getPlanets()
    {
        return $this->planets;
    }

    /**
     * @param ArrayCollection|Planet[] $planets
     * @return User
     */
    public function setPlanets($planets)
    {
        $this->planets = $planets;

        return $this;
    }

    /**
     * @param Planet $planet
     * @return $this
     */
    public function addPlanet(Planet $planet)
    {
        $this->planets->add($planet);

        return $this;
    }

    /**
     * @param Planet $planet
     * @return $this
     */
    public function removePlanet(Planet $planet)
    {
        $this->planets->removeElement($planet);

        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return User
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return ArrayCollection|Science[]
     */
    public function getSciences()
    {
        return $this->sciences;
    }

    /**
     * @param ArrayCollection|Science[] $sciences
     * @return User
     */
    public function setSciences($sciences)
    {
        $this->sciences = $sciences;

        return $this;
    }

    /**
     * @param Science $science
     * @return $this
     */
    public function addScience(Science $science)
    {
        $this->sciences->add($science);

        return $this;
    }

    /**
     * @param Science $science
     * @return $this
     */
    public function removeScience(Science $science)
    {
        $this->sciences->removeElement($science);

        return $this;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return [$this->getRole()];
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     *
     */
    public function eraseCredentials()
    {
    }
}

