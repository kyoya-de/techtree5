<?php

namespace Horiversum\TechTree\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Science
 * @package Horiversum\TechTree\CoreBundle\Entity
 *
 * @ORM\Table(name="science")
 * @ORM\Entity()
 */
class Science
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\User", inversedBy="sciences")
     */
    private $user;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Item")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $item;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Science
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     * @return Science
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return Science
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }
}
