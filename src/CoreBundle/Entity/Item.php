<?php

namespace Horiversum\TechTree\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="item", indexes={@ORM\Index(name="missing_image_search", columns={"has_image"})})
 * @ORM\Entity(repositoryClass="Horiversum\TechTree\CoreBundle\Repository\ItemRepository")
 */
class Item
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=24)
     */
    private $type;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Category", inversedBy="items")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer")
     */
    private $sort;

    /**
     * @var Requirement[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Requirement", mappedBy="item")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $requirements;

    /**
     * @var Requirement[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Requirement", mappedBy="requiredItem")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $prerequisiteFor;

    /**
     * @var string
     *
     * @ORM\Column(name="race", type="string", length=20)
     */
    private $race;

    /**
     * @var int
     *
     * @ORM\Column(name="max_level", type="smallint")
     */
    private $maxLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_image", type="boolean")
     */
    private $image = false;

    /**
     * Item constructor.
     */
    public function __construct()
    {
        $this->requirements    = new ArrayCollection();
        $this->prerequisiteFor = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Item
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get sort
     *
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set sort
     *
     * @param integer $sort
     *
     * @return Item
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     *
     * @return Item
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return ArrayCollection|Requirement[]
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @param ArrayCollection|Requirement[] $requirements
     *
     * @return Item
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * @param Requirement $requirement
     *
     * @return $this
     */
    public function addRequirement(Requirement $requirement)
    {
        $this->requirements->add($requirement);

        return $this;
    }

    /**
     * @param Requirement $requirement
     *
     * @return $this
     */
    public function removeRequirement(Requirement $requirement)
    {
        $this->requirements->removeElement($requirement);

        return $this;
    }

    /**
     * @return ArrayCollection|Requirement[]
     */
    public function getPrerequisiteFor()
    {
        return $this->prerequisiteFor;
    }

    /**
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param string $race
     *
     * @return Item
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxLevel()
    {
        return $this->maxLevel;
    }

    /**
     * @param int $maxLevel
     *
     * @return Item
     */
    public function setMaxLevel($maxLevel)
    {
        $this->maxLevel = $maxLevel;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return Item
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasImage()
    {
        return $this->image;
    }

    /**
     * @param bool $image
     *
     * @return Item
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Item
     */
    public function setType(string $type): Item
    {
        $this->type = $type;

        return $this;
    }
}
