<?php

namespace Horiversum\TechTree\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Availability
 *
 * @ORM\Table(name="building", uniqueConstraints={@ORM\UniqueConstraint(name="unique_level", columns={"item_id", "planet_id"})})
 * @ORM\Entity()
 */
class Building
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Item")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     * @ORM\OrderBy({"category.sort" = "ASC", "sort" = "ASC"})
     */
    private $item;

    /**
     * @var Planet
     *
     * @ORM\ManyToOne(targetEntity="Horiversum\TechTree\CoreBundle\Entity\Planet", inversedBy="buildings")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $planet;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Building
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     * @return Building
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return Planet
     */
    public function getPlanet()
    {
        return $this->planet;
    }

    /**
     * @param Planet $planet
     * @return Building
     */
    public function setPlanet($planet)
    {
        $this->planet = $planet;

        return $this;
    }
}

