<?php

namespace Horiversum\TechTree\CoreBundle\Service;

use Horiversum\TechTree\CoreBundle\Entity\Item;
use Horiversum\TechTree\CoreBundle\Search\NotFoundException;
use Horiversum\TechTree\CoreBundle\Search\ProviderInterface;

class Search
{
    /**
     * @var ProviderInterface[]
     */
    private $providers = [];

    private $sorted = false;

    public function addProvider(ProviderInterface $provider, int $priority): Search
    {
        $this->providers[$priority] = $provider;

        return $this;
    }

    /**
     * @param string $term
     *
     * @return Item[]
     */
    public function search(string $term): array
    {
        if (!$this->sorted) {
            krsort($this->providers);
        }

        $result = [];
        foreach ($this->providers as $provider) {
            try {
                $result = $provider->search($term);
                break;
            } catch (NotFoundException $ignored) {}
        }

        return $result;
    }
}
