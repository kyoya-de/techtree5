<?php

namespace Horiversum\TechTree\MigrationBundle\Command\Migration;

use Doctrine\DBAL\DriverManager;
use Horiversum\TechTree\CoreBundle\Entity\Category;
use Horiversum\TechTree\CoreBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CategoriesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tt:migrate:categories')
            ->setDescription('Migrates all categories from the old TechTree.')
/*            ->addOption(
                'driver',
                'd',
                InputOption::VALUE_REQUIRED,
                'Driver to be used with Doctrine\'s DBAL.',
                'pdo_mysql'
            )
            ->addOption(
                'host',
                'H',
                InputOption::VALUE_REQUIRED,
                'MySQL host',
                'localhost'
            )
            ->addOption(
                'user',
                'u',
                InputOption::VALUE_REQUIRED,
                'MySQL user'
            )
            ->addOption(
                'password',
                'p',
                InputOption::VALUE_REQUIRED,
                'MySQL password'
            )
            ->addOption(
                'port',
                'P',
                InputOption::VALUE_REQUIRED
            )
            ->addOption(
                'charset',
                'c',
                InputOption::VALUE_REQUIRED,
                'MySQL connection charset',
                'utf8'
            )
            ->addArgument(
                'database',
                InputArgument::REQUIRED,
                'MySQL database'
            )*/
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $db = DriverManager::getConnection([
            'driver'   => $input->getOption('driver'),
            'host'     => $input->getOption('host'),
            'user'     => $input->getOption('user'),
            'password' => $input->getOption('password'),
            'port'     => $input->getOption('port'),
            'charset'  => $input->getOption('charset'),
            'dbname'   => $input->getArgument('database'),
        ]);

        $selectCategoriesState = $db->prepare(
            "SELECT dname, lft, rgt
            FROM `tt_units`
            WHERE `type` = 'category' AND lft BETWEEN :lft AND :rgt
            ORDER BY lft ASC"
        );

        $selectItemsState = $db->prepare(
            "SELECT *
            FROM `tt_units`
            WHERE `type` = 'item' AND lft BETWEEN :lft AND :rgt
            ORDER BY lft ASC"
        );

        $stmt = $db->query("SELECT name, dname, lft, rgt FROM `tt_units` WHERE `type` = 'type' ORDER BY lft ASC");

        $em = $this->getContainer()->get('doctrine')->getManager();

        while (false !== ($root = $stmt->fetch())) {
            $output->writeln("Processing root {$root['dname']}");
            $selectCategoriesState->execute(['lft' => $root['lft'], 'rgt' => $root['rgt']]);

            $categoryRowNum = 1;
            while (false !== ($oldCat = $selectCategoriesState->fetch())) {
                $output->writeln("  Processing category {$oldCat['dname']}");
                $cat = new Category();
                $cat->setRoot($root['name'])->setTitle($oldCat['dname'])->setSort($categoryRowNum++);

                $selectItemsState->execute(['lft' => $oldCat['lft'], 'rgt' => $oldCat['rgt']]);
                $em->persist($cat);

                $itemRowNum = 1;
                while (false !== ($oldItem = $selectItemsState->fetch())) {
                    $output->writeln("      Processing item {$oldItem['dname']}");
                    $item = new Item();
                    $item
                        ->setName($oldItem['name'])
                        ->setSort($itemRowNum++)
                        ->setTitle($oldItem['dname'])
                        ->setComment((string) $oldItem['comment'])
                        ->setRace($oldItem['race'])
                        ->setCategory($cat)
                        ->setMaxLevel($oldItem['max_level']);
                    $em->persist($item);
                }

                $em->flush();
            }
        }
    }
}
