<?php

namespace Horiversum\TechTree\ApiBundle\Controller;

use Horiversum\TechTree\CoreBundle\Entity\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RequirementsController extends Controller
{
    const STATUS_ITEM_MISSING       = -1;
    const STATUS_NO_OP              = 0;
    const STATUS_CREATED            = 1;
    const STATUS_UPDATED_ITEM_LEVEL = 2;
    const STATUS_UPDATED_REQ_LEVEL  = 3;

    public function postRequirementsAction(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $itemRepo        = $em->getRepository('CoreBundle:Item');
        $requirementRepo = $em->getRepository('CoreBundle:Requirement');

        $item         = $itemRepo->findOneBy(['name' => $request->get('identifier')]);
        $level        = (int) $request->get('level');
        $requirements = (array) $request->get('requirements');

        $result = [
            'item'         => $item->getTitle(),
            'level'        => $level,
            'changes'      => 0,
            'requirements' => [],
        ];

        foreach ($requirements as $requiredItemName => $requiredItemLevel) {
            $requiredItem                              = $itemRepo->findOneBy(['name' => $requiredItemName]);
            $result['requirements'][$requiredItemName] = self::STATUS_NO_OP;

            if (null === $requiredItem) {
                $result['requirements'][$requiredItemName] = self::STATUS_ITEM_MISSING;

                continue;
            }

            $requirement = $requirementRepo->findOneBy(
                [
                    'item'         => $item,
                    'itemLevel'    => $level,
                    'requiredItem' => $requiredItem,
                ]
            );

            if (null === $requirement) {
                $requirement = $requirementRepo->findOneBy(
                    [
                        'item'              => $item,
                        'requiredItem'      => $requiredItem,
                        'requiredItemLevel' => $requiredItemLevel,
                    ]
                );
            }

            if (null === $requirement) {
                $requirement = new Requirement();
                $requirement
                    ->setItem($item)
                    ->setItemLevel($level)
                    ->setRequiredItem($requiredItem)
                    ->setRequiredItemLevel($requiredItemLevel);

                $result['requirements'][$requiredItemName] = self::STATUS_CREATED;
                $result['changes']++;
            }

            if ($level < $requirement->getItemLevel()) {
                $requirement->setItemLevel($level);
                $result['requirements'][$requiredItemName] = self::STATUS_UPDATED_ITEM_LEVEL;
                $result['changes']++;
            }

            if ($requiredItemLevel !== $requirement->getRequiredItemLevel()) {
                $requirement->setRequiredItemLevel($requiredItemLevel);
                $result['requirements'][$requiredItemName] = self::STATUS_UPDATED_REQ_LEVEL;
                $result['changes']++;
            }

            $em->persist($requirement);
        }

        $em->flush();

        return new JsonResponse($result);
    }
}
