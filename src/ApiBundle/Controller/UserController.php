<?php

namespace Horiversum\TechTree\ApiBundle\Controller;

use Horiversum\TechTree\CoreBundle\Entity\Building;
use Horiversum\TechTree\CoreBundle\Entity\Planet;
use Horiversum\TechTree\CoreBundle\Entity\Science;
use Horiversum\TechTree\CoreBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends Controller
{
    public function postBuildingAction(Request $request, ?Planet $planet): JsonResponse
    {
        $this->checkPlanet($planet);

        $em = $this->getDoctrine()->getManager();

//        $planet       = $em->getRepository('CoreBundle:Planet')->find($planetId);
        $itemRepo     = $em->getRepository('CoreBundle:Item');
        $buildingRepo = $em->getRepository('CoreBundle:Building');

        foreach ((array) $request->request->get('levels') as $name => $level) {
            $item = $itemRepo->findOneBy(['name' => $name]);
            if (null === $item) {
                continue;
            }

            $building = $buildingRepo->findOneBy(['item' => $item, 'planet' => $planet]);

            if (null === $building) {
                $building = new Building();
                $building
                    ->setItem($item)
                    ->setPlanet($planet);
            }

            $building->setLevel($level);

            $em->persist($building);
        }

        $em->flush();

        return new JsonResponse();
    }

    /**
     * @param Planet|null $planet
     *
     * @throws AccessDeniedException
     */
    private function checkPlanet(?Planet $planet)
    {
        /** @var User $user */
        $token = $this->get('security.token_storage')->getToken();
        if (!$token) {
            throw new AccessDeniedException('Sie müssen sich anmelden!');
        }
        $user = $token->getUser();

        if (!$planet || $planet->getUser()->getId() !== $user->getId()) {
            throw new AccessDeniedException('Dieser Planet existiert nicht oder gehört nicht zu deinem Account!');
        }
    }

    public function postScienceAction(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $itemRepo    = $em->getRepository('CoreBundle:Item');
        $scienceRepo = $em->getRepository('CoreBundle:Science');

        foreach ((array) $request->request->get('levels') as $name => $level) {
            $item = $itemRepo->findOneBy(['name' => $name]);
            if (null === $item) {
                continue;
            }

            $science = $scienceRepo->findOneBy(['item' => $item]);

            if (null === $science) {
                $science = new Science();
                $science
                    ->setItem($item)
                    ->setUser($user);
            }

            $science->setLevel($level);

            $em->persist($science);
        }

        $em->flush();

        return new JsonResponse();
    }

    public function postPlanetAction(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $planetRepo = $em->getRepository('CoreBundle:Planet');

        foreach ((array) $request->request->get('planets') as $requestPlanet) {
            $planet = $planetRepo->findOneBy(
                [
                    'galaxy'   => $requestPlanet['galaxy'],
                    'system'   => $requestPlanet['system'],
                    'position' => $requestPlanet['position'],
                    'user'     => $user,
                ]
            );

            if (null === $planet) {
                $planet = $planetRepo->findOneBy(['name' => $requestPlanet['name'], 'user' => $user]);
            }

            if (null === $planet) {
                $planet = new Planet();
                $planet->setUser($user);
            }

            $planet
                ->setGalaxy($requestPlanet['galaxy'])
                ->setSystem($requestPlanet['system'])
                ->setPosition($requestPlanet['position'])
                ->setName($requestPlanet['name'])
                ->setImageNo($requestPlanet['imageNo'])
                ->setType($requestPlanet['type']);

            $em->persist($planet);
        }

        $em->flush();

        return new JsonResponse();
    }

    public function getPlanetAction(): JsonResponse
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $planets = [];
        foreach ($user->getPlanets() as $planet) {
            $planets[] = [
                'id'       => $planet->getId(),
                'name'     => $planet->getName(),
                'galaxy'   => $planet->getGalaxy(),
                'system'   => $planet->getSystem(),
                'position' => $planet->getPosition(),
                'type'     => $planet->getType(),
                'image'    => $planet->getImageNo(),
            ];
        }

        return new JsonResponse($planets);
    }
}
